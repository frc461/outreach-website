import React from 'react';

class Navbar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            static: false,
            permanent: this.props.static
        };
        console.log(this.state.permanent)
    }
    handleScroll(e, component) {
        if(!this.props.static) {
            var $ = e.view.$;
            if($(this).scrollTop() > 100) {
                this.setState({static: true});
            } else {
                this.setState({static: false});
            }
        }
    }
    componentDidMount(){
        if(!this.state.permanent) {
            window.addEventListener('scroll', (e) => this.handleScroll(e, this));
        }else {
            this.setState({static: true});
        }
    }
    componentWillUnmount() {
        window.removeEventListener('scroll', (e) => this.handleScroll(e, this));
    }
    render() {
    return(
    <header id="header" className="header-fixed">
        <div className="container">

        <div id="wbi" className="pull-left">
            <a href="#intro"><img src="/img/wbi.png" id="wbi" /></a>
        </div>


        <nav id="nav-menu-container">
            <ul className="nav-menu">

            {/* <li><a href="#Demos">Demos</a></li>
            <li><a href="#Workshops">Workshops</a></li>
            <li><a href="#Mentoring">Mentoring FIRST Teams</a></li>
            <li><a href="#Partnering">Partnering With Local Organizations</a></li>
            <li><a href="#Camps">Running Camps</a></li>
            <li><a href="#Unified">Unified Robotics</a></li> */}
            <li><a href="/">Home</a></li>
            <li><a href="/videos.html">Videos</a></li>
            <li><a href="/gs.html">Girl Scouts</a></li>
            </ul>
        </nav>
        </div>
    </header>
    );
    }
};

export default Navbar;
