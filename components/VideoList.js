import React from 'react'
import Head from 'next/document'
import Link from 'next/link'
import Section from '../components/Section';
import jwt from 'jsonwebtoken'

class VideoComponent extends React.Component {
  constructor(props) {
    super(props)
    console.log("vc:" , this.props.data)
  }
  render() {
    return (
      <div>
      {this.props.data.map((project, k) => <div>
        <Section label={project.title} variant={(k % 2 == 0) ? "true" : "false"}>
          <Section.Title>{project.title}</Section.Title>
          <Section.Subtitle>{project.description}</Section.Subtitle>
          <Link href={{
            pathname: '/video',
            query: {
              id: jwt.sign(project, 'flyingsquid')
            }
          }}>Go to Project</Link>
        </Section>
      </div>)}
      </div>
    )
  }
}


class VideoList extends React.Component {
  constructor(props) {
      super(props)
      this.state = {
        projects: []
      }
  }
  componentDidMount() {
    this.loadProjects()
  }

  updateProjects(projects) {
    console.log(projects)
    this.setState({
      projects: projects
    })
  }

  loadProjects() {
      const client_id = '496555059137-a04o4edge2p7fso3l5fksib56eh2f0ri.apps.googleusercontent.com'
      const api_key = 'AIzaSyBJAJdXmSUj3xZYXCZYZ_9Pes2ig7awTJ8';
      var DISCOVERY_DOCS = ["https://sheets.googleapis.com/$discovery/rest?version=v4"];
      var SCOPES = "https://www.googleapis.com/auth/spreadsheets.readonly";

      var parent = this;

      if(process.browser) {
          const script = document.createElement("script");
          script.src = "https://apis.google.com/js/api.js";

          script.onload = () => {
              gapi.load('client', () => {
                  gapi.client.init({
                      apiKey: api_key,
                      clientID: client_id,
                      discoveryDocs: DISCOVERY_DOCS,
                      scopes: SCOPES
                  }).then(() => {
                      console.log("OK")
                      gapi.client.sheets.spreadsheets.values.get({
                          spreadsheetId: '1xWkqasNdYJj7mPKMxjAgVidII-BOSiLBuBpgPzHK-Fs',
                          range: 'Videos!A2:E'
                      }).then((response) => {
                          var projects = {};
                          response.result.values.forEach((row) => {
                              if(row[1] != '') {
                                  projects[row[0]] = {
                                      description: row[1],
                                      title: row[0],
                                      videos: []
                                  }
                              }
                              projects[row[0]].videos.push({
                                  title: row[2],
                                  url: row[3]
                              })

                          });

                          this.updateProjects(Object.values(projects))
                      })
                  }, (error => {
                      console.log(error);
                  }))
              });
          }

          document.body.appendChild(script);
      }

  }

  render() {
      return (
          <div>
            <VideoComponent data={this.state.projects} />
          </div>
      )
  }
}

export default VideoList
