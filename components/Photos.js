import Gallery from 'react-photo-gallery';

const photos = [
    {
        src: '/img/gallery/mincool.jpg',
        height: 1000,
        width: 1333
    },
    {
        src: '/img/gallery/gallery2.jpg'
    },
    {
        src: '/img/gallery/gallery1.jpg',
        height: 1143,
        width: 2036
    },
    {
        src: '/img/gallery/gallery3.jpg'
    },
    {
        src: '/img/gallery/gallery5.jpg'
    }, 
    {
        src: '/img/gallery/gallery6.jpg',
        height: 1,
        width: 1.4
    }
];

const PhotoGallery = () => (
    <Gallery photos={photos} direction={'column'} columns={4} />
);


export default PhotoGallery;