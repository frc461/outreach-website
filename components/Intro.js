//Recommended to have nonstatic navbar

var Intro = (props) => (
    <section id="intro" style={{background: `linear-gradient(45deg, rgba(255, 170, 0, 0.75), rgba(240, 24, 0, 0.75)), url(${props.image}) center top no-repeat;`}}>

    <div className="intro-text">
      <h2>{props.title}</h2>
      <p>{props.subtitle}</p>
      <p></p>
      {(props.buttonLink) ? <a href={props.buttonLink} className="btn-get-started scrollto">{props.buttonText}</a> : ''}
    </div>
  </section> 
);

export default Intro

