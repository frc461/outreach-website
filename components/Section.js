const Section = (props) => (
    <div id={props.label}>
      <div className="features-row section-bg">
        <div className="container">
          <div className="row">
            <div className="col-12">
                <img className={(props.variant == "true") ? "wow advanced-feature-img-right fadeInRight" : "wow advanced-feature-img-left"} src={props.image} alt="" />
                <div className={(props.variant == "true") ? "wow fadeInLeft" : "wow fadeInRight"}>
                    {props.children}
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
);

Section.Title = (props) => (
    <h2>{props.children}</h2>
);

Section.Subtitle = (props) => (
    <h3>{props.children}</h3>
);

export default Section;