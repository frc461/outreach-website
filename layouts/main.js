import Head from 'next/head';
import Navbar from '../components/Navbar';

const withLayout = (Page) => {
    return () => (
        <div>
            <Head>
            <title>461 WBI - Outreach</title>
            </Head>
            <Navbar />
            <Page />
        </div>
    );
}

export default withLayout;
