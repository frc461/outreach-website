import { useRouter, withRouter } from 'next/router'
import Intro from '../components/Intro'
import Section from '../components/Section'
import React, { useEffect } from 'react'
import jwt from 'jsonwebtoken'
import Head from 'next/head'
import Navbar from '../components/Navbar'

class ProjectPage extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      project: "no"
    }
  }
  componentDidMount() {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const project = jwt.verify(urlParams.get("id"), 'flyingsquid')
    this.setState({
      project: project,
      title: project.title,
      subtitle: project.title,
      videos: project.videos.map((video) => (
        <Section label="Video" variant="true">
          <Section.Title>{video.title}</Section.Title>
          <iframe width="560" height="315" src={video.url} frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </Section>
      ))
    })
  }
  componentDidUpdate() {
    console.log(this.state.project.videos);
  }
  render() {
    return (
      <>
        <br /> <br /> <br /> <br /> <br /> <br />
        {this.state.videos}
      </>
    )
  }
}

const Page = () => {
  return (
    <>
      <Head>
        <meta name="noLayout"></meta>
      </Head>
      <Navbar static={true} />
      <Intro title="Project Video" subtitle="Enjoy!" />
      <section>
        <ProjectPage />
      </section>
    </>
  )
}

export default Page
