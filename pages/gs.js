import fetch from 'node-fetch'
import Intro from '../components/Intro';
import Section from '../components/Section';

const Page = ({ sections }) => {
    return (
        <div>
            {sections.map((section) => {
                if(section.Type == "p") {
                    return (<Intro title={section["Section Title"]} subtitle={section.Content} buttonLink="" buttonText="" image="/img/background.jpg" />)
                } else if(section.Type == "video_list") {
                    return (<Section label={section["Area ID"]}>
                        <Section.Title>{section["Section Title"]}</Section.Title>
                        <div>
                            <div style={{
                                    display: 'flex',
                                    flexDirection: 'row'
                                }}>
                                {section.videos.map((video) => (
                                
                                    <div style={{
                                        display: 'flex',
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        flexDirection: 'column',
                                        margin: '10px'
                                    }}>
                                        <iframe id="ytplayer" type="text/html" width="320" height="160" src={`https://www.youtube.com/embed/${video["Youtube ID"]}?autoplay=1`} frameborder="0"></iframe>
                                        <p>{video.Title}</p>
                                    </div>
                                ))}
                            </div>
                        </div>
                    </Section>)
                }
            })}
        </div>
    )
}

export async function getStaticProps() {
    const res = await fetch('https://outreach.boilerinvasion.org/api/gs')
    const sections = await res.json()

    return {
        props: {
            sections
        }
    }
}

export default Page