import Head from 'next/head';
import React from 'react';

import Intro from '../components/Intro.js';
import Section from '../components/Section.js'

const Home = () => (
  <React.Fragment>
    <Head>
    </Head>
    <Intro title="WBI 461 - Outreach Guide" subtitle="We love spreading FIRST and STEM, and we'd love to help you spread it as well!" buttonLink="#Demos" buttonText="Get Started" image="/img/background.jpg" />
    
    <section id="advanced-features">
    <Section label="Demos" image="img/demo.jpg" style={false}>
      <Section.Title>Running A Demo!</Section.Title>
      <Section.Subtitle>Running a demo is a great place to start outreach, because you can use your competition robot.</Section.Subtitle>
      <p>First, you should find a place to host it. Libraries, schools, and fairs are common. Next, make sure the robot is safe for kids to drive. We often use our 2016 robot since it shoots soft balls. Additionally, we developed an alternate control system with lower speeds and a master controller that overrides the child’s controller.</p>
      <p>Some kids are scared of large FRC robots so consider bringing or making a smaller robot. We use half size FRC robots that work the same as our full-scale ones and a demo bot built by our FTC teams.</p>
      <p> Here's a presentation we made for a <a target="_blank"href="Curriculums/GeneralCurriculum/robotenge.pptx">robotics and engineering</a> presentation!</p>
    </Section>
    </section>
  </React.Fragment>
);

export default Home;

