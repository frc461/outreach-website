import React from 'react';
import Document, {HTML, Head, Main, NextScript} from 'next/document';
import { loadGetInitialProps } from 'next/dist/next-server/lib/utils';
import defaultLayout from '../layouts/main';
import Intro from '../components/Intro';

var ga = "window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}gtag('js', new Date());gtag('config', 'UA-137364739-2');";

var MainContent = Main;

class MyDocument extends Document {
    static async getInitalProps(ctx) {
        console.log(ctx);
        const initialProps = await Document.getInitialProps(ctx);
        return { ...initialProps }
    }
    constructor(props) {
        super(props);
        // Main.contextType[1]._documentProps.head.forEach((el) => {
        //   console.log(el.props.name == "staticNav")
        // })
        // console.log("document: ", Main.contextType[1]._documentProps.head.find(el => el.props.name === "staticNav").props.name == "staticNav")
        if(Main.contextType[1]._documentProps.head.find(el => el.props.name === "noLayout")) {
            MainContent = Main;
        } else {
          // , (Main.contextType[1]._documentProps.head.find(el => el.props.name === "staticNav").props.name == "staticNav")
            MainContent = defaultLayout(Main);
        }
    }
    render() {
        return(
        <html lang="en">
            <Head>
                <meta charset="utf-8"></meta>
                <meta content="width=device-width, initial-scale=1.0" name="viewport"></meta>

                <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700|Open+Sans:300,300i,400,400i,700,700i" rel="stylesheet"></link>
                {/* <script async defer src="https://apis.google.com/js/api.js" onload="this.onload=function(){};handleClientLoad()" onreadystatechange="if (this.readyState === 'complete') this.onload()"></script> */}
                <script async src="https://www.googletagmanager.com/gtag/js?id=UA-137364739-2"></script>
                <script dangerouslySetInnerHTML={{__html: ga}}></script>
            </Head>
            <body>
                <MainContent />
                <NextScript />
                <script src="/externJs/jquery/jquery.min.js"></script>
                <script src="/externJs/jquery/jquery-migrate.min.js"></script>
                <script src="/externJs/bootstrap/bootstrap.bundle.min.js"></script>
                <script src="/externJs/easing/easing.min.js"></script>
                <script src="/externJs/wow/wow.min.js"></script>
                <script src="/externJs/superfish/hoverIntent.js"></script>
                <script src="/externJs/superfish/superfish.min.js"></script>
                <script src="/externJs/magnific-popup/magnific-popup.min.js"></script>
                <script src="/externJs/main.js"></script>
            </body>
        </html>
        );
    }
};

export default MyDocument;
