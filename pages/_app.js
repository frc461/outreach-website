import '../css/bootstrap/bootstrap.min.css';
import '../css/animate/animate.min.css';
import '../css/font-awesome/css/font-awesome.min.css';
import '../css/ionicons/css/ionicons.min.css';
import '../css/magnific-popup/magnific-popup.css';

import '../css/style.css';

export default function MyApp({Component, pageProps}) {
    return <Component {...pageProps} />
}