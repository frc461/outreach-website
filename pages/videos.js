import Intro from '../components/Intro'
import React from 'react'


import VideoList from '../components/VideoList'

const Videos = () => {
    return (
        <>
            <Intro title="Outreach Videos" subtitle="Due to the pandemic, we have outreach material which you can practice at home!"  image="/img/background.jpg" />
            <VideoList />
        </>
    )
}


export default Videos
